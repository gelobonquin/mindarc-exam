# Midarc Technical Exam

Written on Vue CLI 3 and Talwind CSS

## Installation


```bash
cd mindarc-exam
npm install
npm run serve
```

## Exercise 1

```bash
<base_url>/
<base_url>/exercise-a
```

## Exercise 2

```bash
<base_url>/exercise-b
```
