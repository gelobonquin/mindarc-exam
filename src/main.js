import '@/assets/css/tailwind.css'
import { createApp } from 'vue'
import { h } from 'vue'

/** Routing */
import ExerciseA from './ExerciseA.vue'
import ExerciseB from './ExerciseB.vue'

const routes = {
  '/': ExerciseA,
  '/exercise-a': ExerciseA,
  '/exercise-b': ExerciseB
}

const SimpleRouter = {
  data: () => ({
    currentRoute: window.location.pathname
  }),

  computed: {
    CurrentComponent() {
      return routes[this.currentRoute];
    }
  },

  render() {
    return h(this.CurrentComponent);
  }
}

createApp(SimpleRouter).mount('#app')
